import Link from "next/link";

export default function terms() {
  return (
    <main className="my-10 mx-auto max-w-7xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
      <div>
        <h1 className="text-4xl tracking-normal font-extrabold text-gray-900 sm:text-5xl md:text-6xl text-center">
          <span className="text-white inline">服務條款</span>
        </h1>
        <div className="mt-3 text-base text-white">
          <p>透過使用“黑貓音樂機器人“（以下簡稱本服務），您即同意並遵守本服務的使用條款，同時，您也應遵守“Discord”（以下簡稱該平台）上的<Link href="https://discord.com/terms">社群規範</Link>。</p>
          <br/>
          <h2>一、使用本服務</h2>
          <p>透過使用本服務，任何您在如該平台發布、上傳、分享、儲存的文字、圖片等形式，須遵守該平台的使用條款，並符合法律之規範。</p>
          <br/>
          <p>您同意不會通過服務發布、上傳、共享、存儲或以其他方式提供任何用戶提交的內容：</p>
          <ul>
            <li>侵犯任何第三方的版權或其他權利（例如商標、隱私權等）</li>
            <li>包含色情內容或色情內容</li>
            <li>包含仇恨、誹謗或歧視性、煽動對任何個人或團體的仇恨之內容</li>
            <li>剝削未成年人</li>
            <li>描繪非法行為或極端暴力</li>
            <li>描繪虐待動物或對動物的極端暴力行為</li>
            <li>宣傳欺詐計劃、多層次營銷 (MLM) 計劃、快速致富計劃、在線遊戲和賭博、現金贈送、在家工作或任何其他可疑的賺錢企業</li>
            <li>違反法律或是該平台之使用條款</li>
          </ul>
          <br/>
          <h2>二、隱私權條款</h2>
          <p>使用本服務即表示您已同意本服務之 <Link href="/privacy">隱私權條款</Link></p>
          <br/>
          <h2>三、隱私權保護政策之修正</h2>
          <p>本網站服務條款因應需求隨時進行修正，修正後的條款將刊登於網站上。</p>
        </div>
      </div>
    </main>
  )
}
