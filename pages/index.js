import Link from "next/link"
import Head from "next/head"
import {
  LightningBoltIcon,
  ShieldCheckIcon,
  CodeIcon,
  ChatAlt2Icon
} from "@heroicons/react/solid"

const features = [
  {
    name: "快速載入音樂",
    description:
      "只需要幾秒鐘音樂就會開始播放，無須等候！",
    icon: LightningBoltIcon,
  },
  {
    name: "我們尊重您的隱私",
    description:
      "我們不會紀錄任何您的敏感資訊，也不會刻意去取得您的使用者與伺服器資訊",
    icon: ShieldCheckIcon,
  },
  {
    name: "機器人為完全開源",
    description:
      "黑貓的程式碼全部都可以在GitHub上找到，歡迎您自己下載使用或者是建立新功能",
    icon: CodeIcon,
  },
  {
    name: "全中文界面",
    description:
      "我們提供了全中文的界面，讓您可以更容易的使用黑貓並聆聽您喜愛的音樂",
    icon: ChatAlt2Icon,
  },
]

const Home = () => {
  return (
    <>
      <Head>
        <meta charSet="utf-8" />

        <title>黑貓 - 建立一個更好的音樂伺服器</title>
        <meta name="theme-color" content="#15202B" />
        <meta name="title" content="黑貓 - 建立一個更好的音樂伺服器" />
        <meta name="description" content="Discord中文社群中第一名的音樂機器人，擁有豐富的功能，讓您可以輕鬆的使用黑貓並聆聽您喜愛的音樂" />
        <meta name="canonical" content="https://catmusic.ml" />

        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://catmusic.ml/" />
        <meta property="og:title" content="黑貓 - 建立一個更好的音樂伺服器" />
        <meta property="og:description" content="Discord中文社群中第一名的音樂機器人，擁有豐富的功能，讓您可以輕鬆的使用黑貓並聆聽您喜愛的音樂" />
        <meta property="og:image" content="https://catmusic.ml/banner.jpeg" />

        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://catmusic.ml/" />
        <meta property="twitter:title" content="黑貓 - 建立一個更好的音樂伺服器" />
        <meta property="twitter:description" content="Discord中文社群中第一名的音樂機器人，擁有豐富的功能，讓您可以輕鬆的使用黑貓並聆聽您喜愛的音樂" />
        <meta property="twitter:image" content="https://catmusic.ml/banner.jpeg" />
      </Head>

      <main className="my-10 mx-auto max-w-7xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
        <div className="text-center">
          <h1 className="text-4xl tracking-normal font-extrabold text-gray-900 sm:text-5xl md:text-6xl my-3">
            <span className="text-white inline">建立一個</span>
            <span className="bg-gradient-to-r from-violet-500 to-fuchsia-500 text-transparent bg-clip-text inline">更好的音樂伺服器</span>
          </h1>
          <p className="text-center mt-3 text-base text-gray-400 sm:mt-5 sm:text-lg sm:mx-auto md:mt-5 md:text-xl lg:mx-0">
            Discord中文社群中第一名的音樂機器人，擁有豐富的功能，讓您可以輕鬆的使用黑貓並聆聽您喜愛的音樂
          </p>
          <div className="mt-5 sm:mt-8 sm:flex justify-center">
            <div className="rounded-md shadow">
              <Link href="https://go.catmusic.ml/bot">
                <a
                  className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10 transition"
                >
                  開始使用
                </a>
              </Link>
            </div>
            <div className="mt-3 sm:mt-0 sm:ml-3">
              <Link href="https://discord.gg/fumVBww">
                <a
                  className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-indigo-600 bg-white hover:bg-indigo-50 md:py-4 md:text-lg md:px-10 transition"
                >
                  或是加入我們的Discord伺服器
                </a>
              </Link>
            </div>
          </div>
        </div>
      </main>

      <div className="py-12">
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
          <div className="lg:text-center">
            <h2 className="text-2xl text-indigo-400 font-semibold tracking-wide uppercase">功能</h2>
            <p className="mt-2 text-3xl leading-8 tracking-tight text-gray-100 sm:text-4xl">
              為什麼要使用黑貓?
            </p>
          </div>

          <div className="mt-10">
            <dl className="space-y-10 md:space-y-0 md:grid md:grid-cols-2 md:gap-x-8 md:gap-y-10">
              {features.map((feature) => (
                <div key={feature.name} className="relative">
                  <dt>
                    <div className="absolute flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
                      <feature.icon className="h-6 w-6" aria-hidden="true" />
                    </div>
                    <p className="ml-16 text-lg leading-6 font-medium text-gray-100">{feature.name}</p>
                  </dt>
                  <dd className="mt-2 ml-16 text-base text-gray-400">{feature.description}</dd>
                </div>
              ))}
            </dl>
          </div>
        </div>
      </div>

      <div className="my-8">
        <div className="text-center max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8 lg:flex lg:items-center lg:justify-between">
          <h2 className="text-3xl tracking-tight text-gray-900 sm:text-4xl">
            <span className="block mb-2 text-gray-100">準備好要開始建立屬於你自己的音樂伺服器了嗎?</span>
            <span className="block text-indigo-500">點幾下滑鼠即可開始使用</span>
          </h2>
          <div className="mt-8 flex lg:mt-0 lg:flex-shrink-0 justify-center">
            <div className="inline-flex rounded-md shadow">
              <Link href="https://go.catmusic.ml/bot">
                <a
                  className="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 transition"
                >
                  開始使用
                </a>
              </Link>
            </div>
            <div className="ml-3 inline-flex rounded-md shadow">
              <Link href="https://discord.gg/fumVBww">
                <a
                  className="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-indigo-600 bg-white hover:bg-indigo-50 transition"
                >
                  遇到問題？
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div >
    </>
  )
}

export default Home
