import '../styles/globals.css'
import Nav from '../components/Nav'
import Banner from '../components/Banner'
import Footer from '../components/Footer'
import Script from 'next/script'

function Base({ Component, pageProps }) {
  return (
    <>
      <Script
        id="adobe"
        strategy="beforeInteractive"
      >
        {`
          (function(d) {
            var config = {
              kitId: 'gyo4bgz',
              scriptTimeout: 3000,
              async: true
            },
            h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
          })(document);
        `}
      </Script>
      <Script
        src="https://www.googletagmanager.com/gtag/js?id=G-NC9RB7LJ0C"
        strategy="lazyOnload"
      />
      <Script
        id="analysis"
        strategy="lazyOnload"
      >
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'G-NC9RB7LJ0C');
        `}
      </Script>
      <Nav />
      
      <Component {...pageProps} />
      
      <Footer />
    </>
  )
}

export default Base
