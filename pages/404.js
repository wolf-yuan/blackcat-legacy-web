import Link from "next/link"

export default function notFound() {
  return (
    <main className="my-10 mx-auto max-w-7xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
      <div className="text-center">
        <h1 className="text-4xl tracking-normal font-extrabold text-gray-900 sm:text-5xl md:text-6xl">
          <span className="text-white inline">找不到這個網頁...</span>
        </h1>
        <p className="text-center mt-3 text-base text-gray-400 sm:mt-5 sm:text-lg sm:mx-auto md:mt-5 md:text-xl lg:mx-0">
          找不到這個網頁..有可能是這個網頁不存在，或者是它迷路了
        </p>
        <div className="mt-5 sm:mt-8 sm:flex justify-center">
          <div className="rounded-md shadow">
            <Link href="/">
              <a
                className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10 transition"
              >
                回到首頁
              </a>
            </Link>
          </div>
        </div>
      </div>
    </main>
  )
}
