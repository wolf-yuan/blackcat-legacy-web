import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html lang="zh-Hant">
      <Head>
        <link rel="icon" type="image/png" href="/favicon.png"/>
      </Head>
      <body className="base-font">
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
