import Image from "next/image"
import Link from "next/link"
import { Fragment } from "react"
import { Popover, Transition } from "@headlessui/react"
import { MenuIcon, XIcon, ExternalLinkIcon } from "@heroicons/react/outline"

const navigation = [
  { name: "Discord伺服器", href: "https://discord.gg/fumVBww", target: "_blank" },
  { name: "社群規範", href: "/terms" },
  { name: "隱私權條款", href: "/privacy" }
]

export default function Nav() {
  return (
    <>
      <Popover>
        <div className="relative pt-6 px-4 sm:px-6 lg:px-8">
          <nav className="relative flex items-center justify-between sm:h-10 lg:justify-start" aria-label="Global">
            <div className="flex items-center flex-grow flex-shrink-0 lg:flex-grow-0">
              <div className="flex items-center justify-between w-full md:w-auto">
                <Link href="/" passHref={true}>
                  <>
                    <span className="sr-only">Black cat Logo</span>
                    <div className="h-8 w-8 sm:h-10 sm:w-10 relative">
                      <Image
                        src="/favicon.png"
                        alt="Black cat Logo"
                        width={32}
                        height={32}
                      >
                      </Image>
                    </div>
                  </>
                </Link>
                <div className="-mr-2 flex items-center md:hidden">
                  <Popover.Button className="bg-[#1d2d3d] rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-[#16222e] focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500 transition">
                    <span className="sr-only">開啟主要選單</span>
                    <MenuIcon className="h-6 w-6" aria-hidden="true" />
                  </Popover.Button>
                </div>
              </div>
            </div>
            <div className="hidden md:block md:ml-10 md:pr-4 md:space-x-8">
              {navigation.map((item) => (
                <a key={item.name} href={item.href} target={item.target ?? "_self"} className="font-medium text-gray-300 hover:text-gray-400 transition">
                  {item.name}
                </a>
              ))}
              <Link href="https://go.catmusic.ml/bot">
                <a className="font-medium text-indigo-400 hover:text-indigo-500 transition">
                  開始使用
                </a>
              </Link>
            </div>
          </nav>
        </div>

        <Transition
          as={Fragment}
          enter="duration-150 ease-out"
          enterFrom="opacity-0 scale-95"
          enterTo="opacity-100 scale-100"
          leave="duration-100 ease-in"
          leaveFrom="opacity-100 scale-100"
          leaveTo="opacity-0 scale-95"
        >
          <Popover.Panel
            focus
            className="absolute z-10 top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden"
          >
            <div className="rounded-lg shadow-md bg-[#1d2d3d] ring-1 ring-black ring-opacity-5 overflow-hidden">
              <div className="px-5 pt-4 flex items-center justify-between">
                <div>
                  <div className="h-8 w-8 relative">
                    <Image
                      src="/favicon.png"
                      alt="Black cat logo"
                      width={32}
                      height={32}
                    >
                    </Image>
                  </div>
                </div>
                <div className="-mr-2">
                  <Popover.Button className="bg-[#1d2d3d] rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-[#16222e] focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500 transition">
                    <span className="sr-only">Close main menu</span>
                    <XIcon className="h-6 w-6" aria-hidden="true" />
                  </Popover.Button>
                </div>
              </div>
              <div className="px-2 pt-2 pb-3 space-y-1">
                {navigation.map((item) => (
                  <Link
                    href={item.href}
                    key={item.name}
                  >
                    <a
                      target={item.target ?? "_self"}
                      className=" block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-gray-300 hover:bg-[#172330] transition"
                    >
                      {item.name}
                    </a>
                  </Link>
                ))}
              </div>
              <Link href="https://go.catmusic.ml/bot">
                <a
                  className="block w-full px-5 py-3 text-center font-medium text-indigo-600 bg-[#172330] hover:bg-[#1b2938] transition"
                >
                  開始使用
                </a>
              </Link>
            </div>
          </Popover.Panel>
        </Transition>
      </Popover>
    </>
  )
}
