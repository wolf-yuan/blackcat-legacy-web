import Link from "next/link"
import { useState } from "react"
import { SpeakerphoneIcon, XIcon } from '@heroicons/react/outline'
import { Transition } from "@headlessui/react"

export default function Banner(props) {
  const [showing, setShowing] = useState(true);

  return (
    <Transition
      show={showing}
      leave="transition-opacity duration-500"
      leaveFrom="opacity-100"
      leaveTo="opacity-0"
    >
      <div className="bg-indigo-700 rounded-md mt-4 mx-auto max-w-lg md:max-w-2xl lg:max-w-5xl xl:max-w-[85rem] px-4 sm:px-6 lg:px-8">
        <div className="max-w-7xl mx-auto py-3 px-3 sm:px-6 lg:px-8">
          <div className="flex items-center justify-between flex-wrap">
            <div className="w-0 flex-1 flex items-center">
              <span className="flex p-2 rounded-lg bg-indigo-800">
                <SpeakerphoneIcon className="h-6 w-6 text-white" aria-hidden="true" />
              </span>
              <p className="ml-3 font-medium text-white truncate">
                <span className="md:hidden">{props.title}</span>
                <span className="hidden md:inline">{props.titleLong ?? props.title}</span>
              </p>
            </div>
            {props.href && props.btn &&
              <div className="order-3 mt-2 flex-shrink-0 w-full sm:order-2 sm:mt-0 sm:w-auto">
                <Link href={props.href}>
                  <a
                    className="flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-indigo-600 bg-white hover:bg-indigo-50 transition"
                  >
                    {props.btn}
                  </a>
                </Link>
              </div>
            }
            <div className="order-2 flex-shrink-0 sm:order-3 sm:ml-3">
              <button
                type="button"
                className="-mr-1 flex p-2 rounded-md hover:bg-indigo-500 focus:outline-none focus:ring-2 focus:ring-white sm:-mr-2"
                onClick={() => setShowing(false)}
              >
                <span className="sr-only">關閉</span>
                <XIcon className="h-6 w-6 text-white" aria-hidden="true" />
              </button>
            </div>
          </div>
        </div>
      </div>
    </Transition>
  )
}
